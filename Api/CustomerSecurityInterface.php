<?php
/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_FacebookLogin
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2019 SapientPro (https://sapient.pro)
 */
namespace SapientPro\FacebookLogin\Api;

/**
 * Interface CustomerSecurityInterface
 * @package SapientPro\FacebookLogin\Api
 */
interface CustomerSecurityInterface
{
    /**
     * Save attribute group
     *
     * @return \Magento\Eav\Api\Data\AttributeGroupInterface
     */
    public function auth();

}
