<?php
/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_FacebookLogin
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2019 SapientPro (https://sapient.pro)
 */

namespace SapientPro\FacebookLogin\ViewModel;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Product breadcrumbs view model.
 */
class LoginButton extends DataObject implements ArgumentInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param string $path
     * @return mixed
     */
    public function getConfig(string $path) :string
    {
        return $this->scopeConfig->getValue('sapientpro/facebook/' . $path);
    }

    /**
     * @return string
     */
    public function isLogoutActive() : string
    {
        return $this->getConfig('logout') ? 'true' : 'false';
    }

    /**
     * @return string
     */
    public function isContinueAsActive() : string
    {
        return $this->getConfig('profile') ? 'true' : 'false';
    }

}
