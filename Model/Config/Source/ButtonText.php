<?php
/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_FacebookLogin
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2019 SapientPro (https://sapient.pro)
 */
namespace SapientPro\FacebookLogin\Model\Config\Source;

/**
 * Class ListMode
 * @package Vendor\Module\Model\Config\Source
 */
class ButtonText  implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'login_with', 'label' => __('Login with')],
            ['value' => 'continue_with', 'label' => __('Continue with')],
        ];
    }
}
