<?php
/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_FacebookLogin
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2019 SapientPro (https://sapient.pro)
 */
namespace SapientPro\FacebookLogin\Model;

/**
 * Class ListMode
 * @package Vendor\Module\Model\Config\Source
 */
class CustomerSecurity  implements \SapientPro\FacebookLogin\Api\CustomerSecurityInterface
{
    /**
     * @return bool|\Magento\Eav\Api\Data\AttributeGroupInterface
     */
    public function auth()
    {
        return false;
    }
}
