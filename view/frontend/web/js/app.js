/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_FacebookLogin
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2020 SapientPro (https://sapient.pro)
 */

require(['mage/url', 'domReady!'], function (url) {
    if (window.FB !== undefined) {
        window.FB.Event.subscribe('auth.authResponseChange', function(response) {
            if (response.status) {
                location.href = url.build('facebook/security/auth');
            }
        });
    }
});
