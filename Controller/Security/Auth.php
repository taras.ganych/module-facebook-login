<?php
/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_FacebookLogin
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2019 SapientPro (https://sapient.pro)
 */

namespace SapientPro\FacebookLogin\Controller\Security;

use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Action\Action;
use Magento\Store\Model\StoreManagerInterface;
use SapientPro\Facebook\Api\FacebookInterface;
use Facebook\Exceptions\FacebookSDKException;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Store\Model\StoreResolver;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Api\GroupManagementInterface;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Math\Random as MathRandom;

/**
 * Class Index
 * @package SapientPro\Instagram\Controller\Index
 */
class Auth extends Action
{
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var AccountRedirect
     */
    protected $accountRedirect;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Facebook\Facebook|mixed
     */
    private $facebookSdk;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var CustomerInterfaceFactory
     */
    protected $customerFactory;

    /**
     * @var StoreResolver
     */
    protected $storeResolver;

    /**
     * @var GroupManagementInterface
     */
    private $customerGroupManagement;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var MathRandom
     */
    private $mathRandom;

    /**
     * @var PhpCookieManager
     */
    private $phpCookieManager;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param AccountRedirect $accountRedirect
     * @param FacebookInterface $facebook
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerInterfaceFactory $customerFactory
     * @param StoreResolver $storeResolver
     * @param StoreManagerInterface $storeManager
     * @param GroupManagementInterface $customerGroupManagement
     * @param PhpCookieManager $phpCookieManager
     * @param MathRandom $mathRandom
     * @param ScopeConfigInterface $scopeConfig
     * @param CookieMetadataFactory $cookieMetadataFactory
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        AccountRedirect $accountRedirect,
        FacebookInterface $facebook,
        CustomerRepositoryInterface $customerRepository,
        CustomerInterfaceFactory $customerFactory,
        StoreResolver $storeResolver,
        StoreManagerInterface $storeManager,
        GroupManagementInterface $customerGroupManagement,
        PhpCookieManager $phpCookieManager,
        MathRandom $mathRandom,
        ScopeConfigInterface $scopeConfig,
        CookieMetadataFactory $cookieMetadataFactory
    )
    {
        $this->session = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->accountRedirect = $accountRedirect;
        $this->facebookSdk = $facebook->getSdk();
        $this->customerRepository = $customerRepository;
        $this->storeResolver = $storeResolver;
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->customerGroupManagement = $customerGroupManagement;
        $this->phpCookieManager = $phpCookieManager;
        $this->mathRandom = $mathRandom;
        $this->scopeConfig = $scopeConfig;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        parent::__construct($context);
    }


    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Forward|Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // Is logged in?
        try {
            // Get facebook account
            $helper = $this->facebookSdk->getJavaScriptHelper();
            $response = $this->facebookSdk->get(
                '/me?fields=id,first_name,last_name,email',
                $helper->getAccessToken()
            );
            $fbUser = $response->getGraphUser();
            $firstName = $fbUser->getFirstName();
            $lastName = $fbUser->getLastName();
            $email = $fbUser->getField('email', $fbUser->getId() . '@facebook.com');

            $store = $this->storeManager->getStore();
            $storeId = $store->getId();

            // If customer is logged in, just get customer
            if ($this->session->isLoggedIn()) {
                $customer = $this->customerRepository->getById($this->session->getCustomer()->getId());
                // Bind accounts
                $customer->setFirstname($firstName);
                $customer->setLastname($lastName);
                $customer->setEmail($email);

                $customer->setCustomAttribute('sapientpro_facebook_id', $fbUser->getId());
                $this->customerRepository->save($customer);

            } else {

                // Verify customer exists
                if ($this->customerAccountManagement->isEmailAvailable($email)) {
                    // Create new account
                    $customerDataObject = $this->customerFactory->create();

                    $customerDataObject->setGroupId(
                        $this->customerGroupManagement->getDefaultGroup($storeId)->getId()
                    );

                    $customerDataObject->setWebsiteId($store->getWebsiteId());
                    $customerDataObject->setStoreId($storeId);
                    $customerDataObject->setEmail($email);
                    $customerDataObject->setFirstname($firstName);
                    $customerDataObject->setLastname($lastName);
                    $customerDataObject->setAddresses([]);
                    $customerDataObject->setCustomAttribute('sapientpro_facebook_id', $fbUser->getId());
                    $customer = $this->customerAccountManagement->createAccount(
                        $customerDataObject,
                        $this->mathRandom->getRandomString(10),
                        $this->session->getBeforeAuthUrl()
                    );

                    $this->_eventManager->dispatch(
                        'customer_register_success',
                        ['account_controller' => $this, 'customer' => $customer]
                    );
                } else {
                    $customer = $this->customerRepository->get($email);
                }
            }

            // Get customer facebook ID
            $customerFbId = $customer->getCustomAttribute('sapientpro_facebook_id')->getValue();

            // Whether the user checked in during registration?
            if (!empty($customerFbId) && ($customerFbId == $fbUser->getId())) {
                // Log In
                $this->session->setCustomerDataAsLoggedIn($customer);

                if ($this->phpCookieManager->getCookie('mage-cache-sessid')) {
                    $metadata = $this->cookieMetadataFactory->createCookieMetadata();
                    $metadata->setPath('/');
                    $this->phpCookieManager->deleteCookie('mage-cache-sessid', $metadata);
                }

                $redirectUrl = $this->accountRedirect->getRedirectCookie();
                if (!$this->scopeConfig->getValue('customer/startup/redirect_dashboard') && $redirectUrl) {
                    $this->accountRedirect->clearRedirectCookie();
                    $resultRedirect = $this->resultRedirectFactory->create();
                    // URL is checked to be internal in $this->_redirect->success()
                    $resultRedirect->setUrl($this->_redirect->success($redirectUrl));
                    return $resultRedirect;
                }
            } else {
                throw new LocalizedException(__('Looks like your account has already been created.'));
            }

        } catch (FacebookSDKException | NoSuchEntityException | LocalizedException $e) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('*/*/');
            $this->messageManager->addErrorMessage(
                __('An unspecified error occurred. Please contact us for assistance.')
            );
            return $resultRedirect;
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($this->_redirect->success($this->_redirect->getRefererUrl()));
        return $resultRedirect;
    }
}
